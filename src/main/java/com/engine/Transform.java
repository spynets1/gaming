package com.engine;

import com.engine.GameObject;

/**
 * Transform
 */
public class Transform {
    public GameObject gameObject;

    public Vector position = new Vector();
    public Vector localPosition = new Vector();

    public Vector scale = new Vector(100, 100);
    public Vector firstScale = new Vector(100,100);

    public float rotation = 0;

    public Vector getPosition() {
        if(this.gameObject.parent != null){
            double rotation = Math.toRadians(gameObject.parent.transform.rotation);
            double matrix[][] = {{Math.cos(rotation),-Math.sin(rotation)},{Math.sin(rotation),Math.cos(rotation)}};

            return(this.gameObject.parent.transform.getPosition().add(this.localPosition.multiply(getScale().divide(firstScale)).matrixMultiplication(matrix)));
        }
        return this.position;
    }

    public double getRotation() {
        if(this.gameObject.parent != null){
            return this.rotation+this.gameObject.parent.transform.getRotation();
        }
        return rotation;
    }

    public void setPosition(Vector position) {
        this.position = position;
    }

    public Vector getScale(){
        if(this.gameObject.parent != null){
            return this.scale.multiply(this.gameObject.parent.transform.scale);
        }
        return this.scale;
    }

}
