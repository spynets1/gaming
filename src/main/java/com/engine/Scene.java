package com.engine;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.*;

import com.engine.Game;
import com.engine.Vector;
import com.engine.Components.Renderer;
import com.engine.Input.Input;
import com.engine.Input.Keys;

import java.util.ArrayList;

public class Scene extends JPanel {

    ArrayList<GameObject> gameObjects = new ArrayList<>();
    Vector prevScale = new Vector();
    float scaleFactor = 0.00001f;

    public void update(double deltaTime){
        for(GameObject ob : gameObjects){
            ob.__update(deltaTime);
        }
    }

    public void start(){
        for(GameObject ob : gameObjects){
            ob.__start();
        }
    }


    private void drawDebugStats(Graphics2D g){
        g.drawString("Fps: "+Game.fps,100,80);
        g.drawString("Delta time: "+Game.deltaTime+"ms",100,90);
        g.drawString("Mouse pos: "+Input.getMousePosition().toString(),100,130);
    }

    public static Graphics2D graphics2d;

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if(Input.isKeyPressed(Keys.J)){
            double scaling = scaleFactor/100;
            scaleFactor += scaling;
        }
        if(Input.isKeyPressed(Keys.K)){
            double scaling = scaleFactor/100;
            scaleFactor -= scaling;
        }

        Graphics2D graphics2D = (Graphics2D) g;
        Scene.graphics2d = graphics2D;

        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        // graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        graphics2D.drawString("Scale factor "+scaleFactor, 100, 60);
        drawDebugStats(graphics2D);

        // move everyting to the center
        graphics2D.translate(this.getParent().getWidth()/2, this.getParent().getHeight()/2);

        //scale the grapghics so its is the same size regardless of screen size
        if(prevScale.x != this.getParent().getHeight() &&
                prevScale.y != this.getParent().getHeight()){
            graphics2D.scale(this.getParent().getHeight()*scaleFactor, this.getParent().getHeight()*scaleFactor);
        }

        int x = (int) ((int) (Input.getMousePositionOnCanvas().x / (getParent().getHeight()*scaleFactor) + graphics2D.getClip().getBounds().x));
        int y = (int) ((int) (Input.getMousePositionOnCanvas().y / (getParent().getHeight()*scaleFactor) + graphics2D.getClip().getBounds().y ));

        //divide because we have enlargend the scale by 100
        Input.setMousePosition(new Vector(x/100,y/100));

        for(GameObject ob : gameObjects){
            if(ob.getRenderer() != null) ob.getRenderer().render(graphics2D);
        }
    }
}
