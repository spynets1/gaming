package com.engine;

import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.engine.Components.Renderer;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import com.engine.Input.Input;
import com.engine.SceneHolder;


public class Game extends JFrame
{

    public SceneHolder sceneHolder = new SceneHolder();
    public Scene selectedScene;

    public static int DELAY = 2;
    public  static double deltaTime = 0; // time each second
    private static double prevTime = ((double)System.currentTimeMillis());
    private static double time = System.currentTimeMillis();
    /**
     * this is the amount of frames drawn every second
     */
    public  static float fps = 5;
    private static float counter = 5;
    /**
     * This caps the amount of frames drawn in a second (0 = uncapped)
     */
    public static float fpsCap = 60;
    long elapsedTime = 0;

    public Game(){
        System.setProperty("sun.java2d.opengl", "true");

        this.setTitle("Game");
        this.setSize(1280,720);

        setSelectedScene(new Scene());

        this.setContentPane(sceneHolder);
        this.setVisible(true);

    }
    public void start(){
        selectedScene.start();
        javax.swing.Timer timer = new javax.swing.Timer(DELAY, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                update();
            }

        });
        timer.setCoalesce(true);
        timer.setRepeats(true);
        timer.start();
    }

    public void update(){

        double now = ((double) System.currentTimeMillis());
        //Increases counter every tick but when a 1/10 of a second we reset the counter
        //and sets the fps to the counter
        // To cap the fps we just increase the delay if our fps is too high
        // and decrease it when it is too low
        if (now - time >= 100) {
            fps = counter * 10;
            if (fpsCap > 0 && fps > fpsCap) DELAY++;
            if (fpsCap > 0 && fps < fpsCap && DELAY > 5) DELAY--;

            counter = 0;
            time = now;
        }
        counter++;

        // delta time is the time from previous frame (tick speed)
        deltaTime = (now - prevTime)/1000;
        prevTime = now;


        Toolkit.getDefaultToolkit().sync();
        selectedScene.validate();
        selectedScene.repaint();

        selectedScene.update(deltaTime);
    }

    public Scene getSelectedScene(){
        return selectedScene;
    }

    public void setSelectedScene(Scene selectedScene){
        if(this.selectedScene != null)
            sceneHolder.remove(this.selectedScene);
        this.selectedScene = selectedScene;
        sceneHolder.add(selectedScene);
        update();
    }

}
