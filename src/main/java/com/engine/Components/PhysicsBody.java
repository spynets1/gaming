package com.engine.Components;

import com.engine.Component;

/**
 * PhysicsBody
 */
public class PhysicsBody extends Component{
    float ac = 0;

    public void update(double deltaTime){
        if(parent.transform.getPosition().y > -100)
            parent.transform.getPosition().y += ac*deltaTime;
        ac += 9.8;
    }
}
