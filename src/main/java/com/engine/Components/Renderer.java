package com.engine.Components;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import com.engine.Component;
import com.engine.GameObject;
import com.engine.Transform;
import com.engine.Vector;

/**
 * Renderer
 */
public class Renderer extends Component {

    public Color color = Color.DARK_GRAY;

    public Renderer() {
        parent = null;
    }

    public void rotatePoints(int[] xpoints, int[] ypoints, Transform transform) {

        double px = transform.getPosition().x * 100;
        double py = transform.getPosition().y * 100;

        double angle = transform.getRotation();

        double radians = Math.toRadians(angle);
        double cosTheta = Math.cos(radians);
        double sinTheta = Math.sin(radians);

        for (int i = 0; i < xpoints.length; i++) {
            int x = xpoints[i];
            int y = ypoints[i];

            // Rotate point
            double newX = (x * cosTheta - y * sinTheta);
            double newY = (x * sinTheta + y * cosTheta);

            // Translate point back
            xpoints[i] = (int) (newX + px);
            ypoints[i] = (int) (newY + py);
        }
    }

    public void pivotPoints(int xpoints[], int ypoints[], Transform transform) {
        for (int i = 0; i < xpoints.length; i++) {
            xpoints[i] += (-transform.getScale().x * 100/ 2);
            ypoints[i] += (-transform.getScale().y  * 100/ 2 );
        }
    }

    public void render(Graphics2D g) {

        Color cb = g.getColor();
        g.setColor(color);

        for (GameObject child : this.parent.children) {
            if (child.getRenderer() != null) {
                child.getRenderer().render(g);
            }
        }

        Transform transform = this.parent.transform;

        Polygon p = new Polygon();
        p.npoints = 4;

        p.xpoints = new int[] { 0, (int) transform.getScale().x * 100, (int) transform.getScale().x* 100, 0 };
        p.ypoints = new int[] { 0, 0, (int) transform.getScale().y* 100, (int) transform.getScale().y* 100 };


        pivotPoints(p.xpoints, p.ypoints, transform);
        rotatePoints(p.xpoints, p.ypoints, transform);


        g.fillPolygon(p);

        Font font = new Font("Serif", Font.PLAIN, 1000);
        g.setFont(font);
        g.drawString("Pos "+this.parent.transform.getPosition(), (int) parent.transform.getPosition().x*100, (int) (parent.transform.getPosition().y + 90)*100);

        g.setColor(cb);
    }
}
