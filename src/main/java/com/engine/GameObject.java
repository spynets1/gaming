package com.engine;

import java.util.LinkedList;

import javax.swing.text.Position;

import com.engine.Components.Renderer;
import com.engine.Input.Input;
import com.engine.Input.Keys;

import lombok.Getter;
import lombok.Setter;

/**
 * GameObject
 */
public class GameObject {
    public GameObject parent = null;

    public LinkedList<GameObject> children = new LinkedList<>();
    public LinkedList<Component> components = new LinkedList<>();
    public Transform transform = new Transform();

    @Getter
    private Renderer renderer = null;

    public GameObject() {
        transform.gameObject = this;
    }

    public void setRenderer(Renderer renderer) throws Exception {
        if (this.renderer == null)
            throw new Exception("No renderer attached to this gameobject");
        this.renderer = renderer;
    }


    public void __update(double deltaTime) {
        for (int i = 0; i < components.size(); i++) {
            components.get(i).update(deltaTime);
        }

        for (int i = 0; i < children.size(); i++) {
            children.get(i).__update(deltaTime);
        }
        update(deltaTime);
    }

    public void addComponent(Component comp) {
        if (comp.getClass() == Renderer.class)
            this.renderer = (Renderer) comp;

        comp.parent = this;
        components.add(comp);
        //start the component
        comp.start();
    }
    public <T extends Component> T getComponent(Class<T> type) {

        for (Component c : components) {
            if (type.isInstance(c)) {
                return (T) c;
            }
        }
        return null;
    }

    public void addChild(GameObject child) {
        child.parent = this;
        children.add(child);
        child.__start();
    }

    public void __start(){
        transform.firstScale = transform.getScale();
        start();
    }

// ##################################
// ||                              ||
// ||    USER OVERIDE FUNCTIONS    ||
// ||                              ||
// ##################################
    public void start(){}

    public void update(double deltaTime){}

}
