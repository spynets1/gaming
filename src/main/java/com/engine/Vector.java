package com.engine;

/**
 * Vector
 */
public class Vector {
    public double x = 0;
    public double y = 0;

    public Vector(){

    }

    public Vector(double x, double y){
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("{%f,%f}",x,y);
    }

    public Vector add(Vector vec1){
        return new Vector(x+vec1.x,y+vec1.y);
    }

    public double dot(Vector vec1){
        return this.x*vec1.x + this.y * vec1.y;
    }

    public Vector multiply(double multiple) {
        return new Vector( (x*multiple),  (y*multiple));
    }
    public Vector multiply(Vector vec1) {
        return new Vector( (x*vec1.x),  (y*vec1.y));
    }

    public Vector matrixMultiplication(double[][] matrix) {
        if (matrix == null || matrix.length != 2 || matrix[0].length != 2 || matrix[1].length != 2) {
            throw new IllegalArgumentException("Matrix must be a 2x2 matrix.");
        }

        Vector vec = new Vector();
        vec.x = (this.x * matrix[0][0] + this.y * matrix[0][1]);
        vec.y = (this.x * matrix[1][0] + this.y * matrix[1][1]);
        return vec;
    }
    public Vector divide(Vector vec1){
        return new Vector(this.x/vec1.x,this.y/vec1.y);
    }
}
