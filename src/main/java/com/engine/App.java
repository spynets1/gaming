package com.engine;

import com.engine.Components.PhysicsBody;
import com.engine.Components.Renderer;
import com.engine.Input.Input;
import com.engine.Input.Keys;

import java.awt.Color;

import com.engine.GameObject;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Game game = new Game();
        Scene scene = game.getSelectedScene();

        GameObject gb = new GameObject(){
                public void update(double deltaTime)   {
                    super.update(deltaTime);
                    if(Input.isKeyDown(Keys.A)){
                        this.transform.getPosition().x -= 500 * Game.deltaTime;
                    }
                    if(Input.isKeyDown(Keys.D)){
                        this.transform.getPosition().x += 500 * Game.deltaTime;
                    }
                    if(Input.isKeyDown(Keys.W)){
                        this.transform.getPosition().y -= 500 * Game.deltaTime;
                    }
                    if(Input.isKeyDown(Keys.S)){
                        this.transform.getPosition().y += 500 * Game.deltaTime;
                    }
                    if(Input.isKeyDown(Keys.E)){
                        transform.rotation += 1;
                    }
                    if(Input.isKeyDown(Keys.Q)){
                        transform.rotation -= 1;
                    }
                    if(Input.isKeyDown(Keys.F)){
                        transform.scale.y --;
                        transform.scale.x --;
                    }
                    if(Input.isKeyDown(Keys.G)){
                        transform.scale.y ++;
                        transform.scale.x ++;
                    }
                    if(Input.isKeyDown(Keys.SPACE)){
                        children.get(0).transform.scale.y += 0.01f;
                        children.get(0).transform.scale.x += 0.01f;
                    }
                }
            };

        gb.addComponent(new Renderer());
        gb.addComponent(new PhysicsBody());

        GameObject child = new GameObject();
        child.addComponent(new Renderer());
        child.getComponent(Renderer.class).color = Color.RED;
        child.transform.localPosition = new Vector(100,0);
        child.transform.scale = new Vector(1,0.5);
        gb.addChild(child);


        GameObject child1 = new GameObject();
        child1.addComponent(new Renderer());
        child1.getComponent(Renderer.class).color = Color.RED;
        child1.transform.localPosition = new Vector(-100,0);
        child1.transform.scale = new Vector(1,1);
        // gb.addChild(child1);

        scene.gameObjects.add(gb);

        game.start();


        Vector vector = new Vector(0,1);
        double rot = Math.PI/4;
        double matrix[][] = {{Math.cos(rot),-Math.sin(rot)},{Math.sin(rot),Math.cos(rot)}};
        System.out.println(vector.matrixMultiplication(matrix));

    }
}
