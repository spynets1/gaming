package com.engine;

import java.awt.Graphics2D;

/**
 * Component
 */
public abstract class Component {
    public GameObject parent;

// ##################################
// ||                              ||
// ||    USER OVERIDE FUNCTIONS    ||
// ||                              ||
// ##################################
    public void update(double deltaTime){}
    public void start(){}

}
