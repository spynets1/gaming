package com.engine;

import com.engine.Input.Input;
import com.engine.Vector;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * SceneHolder is a holder for the scenes. It is also responsible to send all user inputs to the input class
 */
public class SceneHolder extends JPanel {

    public SceneHolder(){
        setLayout(new GridLayout(0, 1));
        setBackground(Color.BLUE);
        setFocusable(true);


        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                Input.addKey(e);
            }
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                Input.removeKey(e);
            }
        });

        /*
          mouse input
         */
        MouseAdapter mouseAdapter = new MouseAdapter(){
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                Input.addMouseButton(e);
                Input.setMouseEvent(e);
                Input.setMousePressed(e.getButton());
                requestFocus();
            }
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                Input.removeMouseButton(e);
                Input.setMouseEvent(e);

            }
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                Input.setMousePositionOnCanvas(new Vector(e.getPoint().x,  e.getPoint().y));
                Input.setMouseEvent(e);

            }
            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);
                Input.setMousePositionOnCanvas(new Vector( e.getPoint().x,  e.getPoint().y));
                Input.setMouseEvent(e);
                requestFocus();
            }
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                // Input.setScrollValue( e.getPreciseWheelRotation());
                Input.setMouseEvent(e);
            }

        };
        addMouseListener(mouseAdapter);
        addMouseMotionListener(mouseAdapter);
        addMouseWheelListener(mouseAdapter);

    }
}
